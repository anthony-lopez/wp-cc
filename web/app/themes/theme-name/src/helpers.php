<?php

use Symfony\Component\VarDumper\Dumper\CliDumper;
use Symfony\Component\VarDumper\Dumper\HtmlDumper;

if ( ! function_exists( 'dd' ) ) {
    /**
     * Dump the passed variables and end the script.
     *
     * @param  mixed $args
     *
     * @return void
     */
    function dd( ...$args ) {
        http_response_code( 500 );
        foreach ( $args as $x ) {
            $dumper = in_array( PHP_SAPI, [ 'cli', 'phpdbg' ] ) ? new CliDumper : new HtmlDumper;

            ( new $dumper )->dump(
                ( new \Symfony\Component\VarDumper\Cloner\VarCloner() )->cloneVar( $x )
            );
        }
        die( 1 );
    }
}

if ( ! function_exists( 'asset' ) ) {
    /**
     * @param string $path
     *
     * @return string
     */
    function asset( $path ) {
        return get_template_directory_uri() . '/' . $path;
    }
}
