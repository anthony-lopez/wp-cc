<?php

/**
 * Replace all occurrences of the search string with the replacement string in the specified file
 *
 * @param mixed $search
 * @param mixed $replace
 * @param string $file File path
 */
function str_replace_file( $search, $replace, $file ) {
    $fileContent = file_get_contents( $file );
    $fileContent = str_replace( $search, $replace, $fileContent );
    file_put_contents( $file, $fileContent );
}

/**
 * @param string $string
 *
 * @return string
 */
function kebab_case( $string ) {
    return strtolower( preg_replace( '%([a-z])([A-Z])%', '\1-\2', $string ) );
}

/**
 * @param $string
 * @param bool $capitalizeFirstCharacter
 *
 * @return mixed|string
 */
function camel_case( $string, $capitalizeFirstCharacter = false ) {
    $str = str_replace( '-', '', ucwords( $string, '-' ) );
    if ( ! $capitalizeFirstCharacter ) {
        $str = lcfirst( $str );
    }

    return $str;
}

$projectName       = kebab_case( basename( __DIR__ ) );
$themeDirectory    = __DIR__ . '/web/app/themes/theme-name';
$newThemeDirectory = __DIR__ . '/web/app/themes/' . $projectName;

// Rename theme directory with the project name
if ( file_exists( $themeDirectory ) ) {
    rename( $themeDirectory, $newThemeDirectory );
}
$themeDirectory = $newThemeDirectory;

// Update theme name in project files
str_replace_file( '{theme-name}', $projectName, $themeDirectory . '/style.css' );
str_replace_file( '{theme-name}', $projectName, $themeDirectory . '/webpack.mix.js' );
str_replace_file( '{theme-name}', $projectName, __DIR__ . '/composer.json' );
str_replace_file( '{ThemeName}', camel_case( $projectName, true ), __DIR__ . '/composer.json' );

// Self destruct
unlink( __DIR__ . '/setup.php' );